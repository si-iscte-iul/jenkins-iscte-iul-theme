function forceLogoutButton() {
  var login = document.getElementsByClassName("login").item(0); 
  var logout = login.children[0].cloneNode(true); 
  logout.children[0].setAttribute('href',"/logout"); 
  logout.children[0].innerHTML ="logout"; 
  login.appendChild(logout);
}
document.addEventListener('DOMContentLoaded', forceLogoutButton, false);
