# README #

This is a repository with a generated to theme for internal ISCTE-IUL jenkins.

Place this content on following folder within jenkins.
/var/lib/jenkins/userContent

Install simple theme plugin on jenkins.

Configure the css and javascript on jenkins:
Manage Jenkins > Configure jenkins > Theme
URL of theme CSS: /userContent/jenkins-material-theme.css
URL of theme JS: /userContent/logout_force_to_add.js

Thanks to http://afonsof.com/jenkins-material-theme/
